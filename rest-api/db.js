export default {
    categories: [
        {
            id:1,
            title: 'Languges'
        },
        {
            id:2,
            title: 'DevOps'
        },
        {
            id: 3,
            title: 'Editors'
        }
    ],
    articles: [
        {
            id: 1,
            title: 'Introduction to JavaScript',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 1
        },
        {
            id: 2,
            title: 'Introduction to Python',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 1
        },
        {
            id: 3,
            title: 'Introduction to Elixir',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 2
        },
        {
            id: 4,
            title: 'Introduction to Ruby',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 3
        },
        {
            id: 5,
            title: 'Introduction to Docket',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 3
        }
        ,
        {
            id: 6,
            title: 'Introduction to C#',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 3
        }
        ,
        {
            id: 7,
            title: 'Introduction to JAVA',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 3
        }
        ,
        {
            id: 8,
            title: 'Introduction to C++',
            content: 'Lorem Ipsum',
            authorId: 1,
            categoryId: 3
        }
    ],
    users: [
        {
            id: 1, 
            email: 'babelcoder@gmail.com',
            isAdmin: true,
            password: '$2a$12$.rjS5eXZpVo3aC2TuRsV0.8qzBOVPM01zguiMTKwb1BYxIVR7PtQS'
        },
        {
            id: 2, 
            email: 'somchai@gmail.com',
            isAdmin: true,
            password: '$2a$13$ZyprE5MRw2Q3WpNOGZWGbeG7ADUre1Q8QO.uUUtcbqloU0yvzavOm'
        }
    ]
}