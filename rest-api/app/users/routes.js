import controller from './controller'

export function setup(router) {
   // router.get('/', (req, res) => {res.send('Hello World')})
   router
    .get('/:id', controller.get)
    .get('/', controller.getAll)
    .post('/', controller.create)

    //console.log('users Pass')
}